# Client-server app
Client-server app is my own server based on sockets where you can use the following commands.
```python
{
    "uptime": "return server life time",
    "info": "return information about server version",
    "help": "return commands list",
    "STOP": "connection closed (client and server)",
    "remove": "deleting any account",
    "show users": "display all users",
    "change password": "change password",
    "display credential": "display credential",
    "send message": "sending message",
    "show messages": "showing messages",
    "clear messages": "removes user messages",
}
```

# Usage
To start this server you need just follow these commands.
```bash
For run tests
-> python -m unittest discover 

For run app in first bash
-> python server.py
In second bash
-> python client.py
```
