"""Manages the data in the database. 
   It uses methods from the sql_queries.py file. """

import psycopg2

# Obtain the configuration parameters in file .gitignore
# from app.src.config import config
from sql_queries import DatabaseSQL

db = DatabaseSQL()

cure = db.create_cursor()


def show_users_data():
    users = db.get_users()
    data = []
    for row in users:
        data.append(row)
    return data


def show_only_usernames():
    usernames = db.get_only_usernames()
    data = []
    for username in usernames:
        data.append(username[0])
    return data


def check_password(username: str, password: str):
    user_password = db.get_password(username)
    if user_password[0] == password:
        return True
    else:
        return False


def check_if_admin(username: str):
    user_rights = db.get_rights(username)
    if "1" == user_rights:
        return True
    return False


def change_password(username: str, old_password: str, new_password: str):
    if check_password(username, old_password):
        db.update_password(username, new_password)
        return True
    else:
        return False


def send_message(
    sender: str, receiver: str, message: str
):  # TODO:user can only have 5 message in box
    receiver = db.get_specific_user(receiver)[1]
    if db.get_count_of_messages(receiver) < 7:
        db.insert_message(sender, receiver, message)
        return True
    else:
        return False


def show_messages(username: str):
    message_list = []
    message_amount = 0
    messages = db.get_specific_messages(username)
    for message in messages:
        message_amount += 1
        message_list.append(str(message_amount) + ". " + message[1] + ": " + message[0])
    return message_list


def clear_messages(administrator: str, username: str):
    """Returns TRUE if the messages of a given user have been deleted in otherwise return FALSE."""
    try:
        username = db.get_specific_user(username)[1]
        user_id = db.get_specific_user(username)[0]
        if check_if_admin(administrator):
            db.delete_message(user_id)
            return True
        else:
            return False
    except IndexError:
        return False


# print(send_message('Jacob', 'Pablo', 'wiadomosc od Jacoba'))
# print(clear_messages('Jacob', 'Pablo'))
# print(check_password("Kuba", "kuba"))
# print(db.get_users())
