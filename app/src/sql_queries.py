"""Manages sql queries."""

import psycopg2
import sqlite3
from sqlite3 import Error

# Obtain the configuration parameters in file .gitignore
from db_config import config


class DatabaseSQL:
    def __init__(self, params=config()):
        self.connection = psycopg2.connect(**params)

    # If you want to use sqlite uncommented this code
    # db_file = "E:\ZaRaczke\jsonsocket\\app\src\sqlite\db\pythonsqlite.sqlite"
    # def __init__(self, db_file):
    #     self.connection = sqlite3.connect(db_file)

    # Postgres documentaction says - "alawys create a new cursor and dispose old"
    def create_cursor(self):
        with self.connection.cursor() as self.cur:
            self.cur = self.connection.cursor()
            return self.cur

    def get_users(self):
        self.create_cursor()
        self.cur.execute("SELECT * FROM users;")
        users = self.cur.fetchall()
        return users

    def get_only_usernames(self):
        self.create_cursor()
        self.cur.execute("SELECT username FROM users;")
        users = self.cur.fetchall()
        return users

    def get_specific_user(self, username: str):
        """:param username: username of user"""
        self.create_cursor()
        self.cur.execute("SELECT * FROM users where username = %s;", (username,))
        specific_user = self.cur.fetchone()
        return specific_user

    def get_password(self, username):
        """:param username: username of user"""
        self.create_cursor()
        try:
            self.cur.execute(
                "SELECT password FROM users where username = %s;", (username,)
            )
            user_credentials = self.cur.fetchone()
            return user_credentials
        except TypeError:
            return False

    def get_rights(self, username):
        """:param username: username of user"""
        self.create_cursor()
        self.cur.execute("SELECT rights FROM users where username = %s;", (username,))
        user_rights = self.cur.fetchone()[0]
        return user_rights

    def delete_specific_user(self, administrator: int, username: str):
        """:param administrator: rights of user
        :param username: username of user"""
        self.create_cursor()
        if (
            self.get_specific_user(username) in self.get_users()
            and self.get_rights(administrator) == "1"
        ):  # TODO: CR - sprawdzenie czy user jest w bazie jest konieczne zapytanei DELETE zawsze zwraca None, nie wiem jak to obsłużyć
            self.cur.execute("DELETE FROM users where username = %s;", (username,))
            self.connection.commit()
            return True
        else:
            return False

    def update_password(self, username: str, new_password: str):
        """:param username: username of use
        :param new_password: new user password"""
        self.cur.execute(
            "UPDATE users SET password = %s where username = %s;",
            (new_password, username),
        )
        self.connection.commit()
        return True

    def get_count_of_messages(self, username: str):
        username = self.get_specific_user(username)
        self.cur.execute(
            "SELECT COUNT(*) FROM messages WHERE user_id = %s;", (username[0],)
        )
        box_size = self.cur.fetchone()[0]
        return box_size

    def insert_message(self, sender: str, receiver: str, message: str):
        """:param sender: user who send message
        :param receiver: user who receive message
        :param message:  message"""
        receiver_id = self.get_specific_user(receiver)[0]
        self.cur.execute(
            "INSERT INTO messages(user_id, message, sender) VALUES (%s, %s, %s);",
            (receiver_id, message, sender),
        )
        self.connection.commit()
        return True

    def get_specific_messages(self, username):
        user_id = self.get_specific_user(username)[0]
        self.cur.execute(
            "SELECT message, sender FROM messages where user_id = %s;", (user_id,)
        )
        messages = self.cur.fetchall()
        return messages

    def delete_message(self, receiver_id: int):
        self.cur.execute("DELETE FROM messages where user_id = %s;", (receiver_id,))
        self.connection.commit()
        return True

    def add_new_user(self, username, password):
        try:
            self.create_cursor()
            self.cur.execute(
                "INSERT INTO users (username, password, rights) VALUES(%s, %s, %s);",
                (username, password, "0"),
            )
            self.connection.commit()
            return True
        except:
            self.connection.rollback()
            return False


db = DatabaseSQL()
# print(db.delete_specific_user('Pablo', 'Angela2'))
# print(db.get_users())
# print(db.get_rights('Pablo'))
# print(db.add_user("Angela2", "angela123"))
# print(db.add_user("Angela", "angela123"))
# print(db.insert_message('Jacob', 'Pablo', 'adsd'))
# print(db.get_count_of_messages("Jacob"))
# print(db.get_specific_messages('Pablo'))
