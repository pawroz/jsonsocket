import json
import socket
from datetime import datetime

from server_commands import Server

# from app.src.sqldatabase import DatabaseSQL
from user import User
from sqldatabase import *

# dbSQL = DatabaseSQL()
sv = Server()


PORT = 7071
SERVER = "127.0.0.1"
ADDR = (SERVER, PORT)

HEADER = 64
FORMAT = "utf-8"
BUFFER = 1024

NOW = datetime.now()
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(ADDR)
    print("[SERVER] Starting...")
    s.listen()
    clientsocket, address = s.accept()
    with clientsocket:
        print(f"[SERVER] Connected by {address}")
        clientsocket.send("Click 1 to Register, or 2 to Log in".encode(FORMAT))
        while True:
            message_from_client = clientsocket.recv(BUFFER).decode(FORMAT)
            print(f"[CLIENT] {message_from_client}")
            if message_from_client == "2":
                clientsocket.send(
                    "Write username and password in this format -> 'username password'".encode(
                        FORMAT
                    )
                )
                credentials = clientsocket.recv(BUFFER).decode(FORMAT).split(" ")
                while len(credentials) != 2:
                    clientsocket.send(
                        "Wrong format, write again username and password with space between for login".encode(
                            FORMAT
                        )
                    )
                    credentials = clientsocket.recv(BUFFER).decode(FORMAT).split(" ")
                if check_password(credentials[0], credentials[1]):
                    clientsocket.send(
                        f"Correct! Welcome {credentials[0]}".encode(FORMAT)
                    )
                    # else:
                    #     clientsocket.send(
                    #         f"Wrong login. Click 1 to Register, or 2 to Log in".encode(FORMAT)
                    #     )
                    while True:
                        uptime = str(datetime.now() - NOW)
                        sv.options_dict_user["uptime"] = uptime
                        sv.options_dict_admin["uptime"] = uptime
                        message_from_client = clientsocket.recv(BUFFER).decode(FORMAT)
                        print(f"[CLIENT] {message_from_client}")

                        if check_if_admin(credentials[0]):
                            options_dict = sv.options_dict_admin
                            user = User(credentials[0], credentials[1], 1)
                        else:
                            options_dict = sv.options_dict_user
                            user = User(credentials[0], credentials[1], 0)
                        if (
                            message_from_client in options_dict
                            and message_from_client != "STOP"
                        ):
                            response = json.dumps(
                                {
                                    message_from_client: options_dict[
                                        message_from_client
                                    ]
                                },
                                indent=2,
                            )
                            clientsocket.send(response.encode(FORMAT))
                        # wprowadzić słownik z funkcjami
                        elif message_from_client == "remove":
                            clientsocket.send(
                                "Write username what you want to remove".encode(FORMAT)
                            )
                            user_remove = clientsocket.recv(BUFFER).decode(FORMAT)
                            if db.delete_specific_user(
                                user_remove, user.username
                            ):  # TODO: sprawdzic dzialanie tej metody
                                clientsocket.send("removed succefully".encode(FORMAT))
                            else:
                                clientsocket.send("removed unsuccefully".encode(FORMAT))
                        elif message_from_client == "show users data":
                            clientsocket.send(
                                json.dumps(show_users_data(), indent=2).encode(FORMAT)
                            )
                        elif (
                            message_from_client == "show users"
                        ):  # pokazuje tylko usernames
                            clientsocket.send(
                                json.dumps(show_only_usernames(), indent=2).encode(
                                    FORMAT
                                )
                            )
                        # elif message_from_client == "display credential":
                        #     clientsocket.send(
                        #         json.dumps(dbSQL.show_data(), indent=2).encode(FORMAT)
                        #     )
                        elif message_from_client == "change password":
                            clientsocket.send(
                                "Write your old password and new password after spaces".encode(
                                    FORMAT
                                )
                            )
                            old_and_new_password = (
                                clientsocket.recv(BUFFER).decode(FORMAT).split(" ")
                            )
                            print(old_and_new_password[0], old_and_new_password[1])
                            try:
                                if change_password(
                                    user.username,
                                    old_and_new_password[0],
                                    old_and_new_password[1],
                                ):
                                    clientsocket.send(
                                        "password changed succefully".encode(FORMAT)
                                    )
                                else:
                                    clientsocket.send(
                                        "password not changed".encode(FORMAT)
                                    )
                            except IndexError:
                                clientsocket.send(
                                    "Wrong format. Choose again command.".encode(FORMAT)
                                )
                        elif message_from_client == "send message":
                            clientsocket.send(
                                "Write receiver and message content".encode(FORMAT)
                            )
                            receiver_and_message = (
                                clientsocket.recv(BUFFER).decode(FORMAT).split(":")
                            )
                            try:
                                if send_message(
                                    user.username,
                                    receiver_and_message[0],
                                    receiver_and_message[1],
                                ):
                                    clientsocket.send(
                                        "message sended succefully".encode(FORMAT)
                                    )
                                else:
                                    clientsocket.send(
                                        "message not sended".encode(FORMAT)
                                    )

                            except IndexError:
                                clientsocket.send(
                                    "Write receiver and message content in this format Username:Message. Choose again command.".encode(
                                        FORMAT
                                    )
                                )
                        elif message_from_client == "show messages":
                            clientsocket.send(
                                json.dumps(
                                    show_messages(user.username), indent=2
                                ).encode(FORMAT)
                            )
                        elif (
                            message_from_client == "clear messages" and user.rights == 1
                        ):
                            clientsocket.send(
                                "Write which users you want to clear message box".encode(
                                    FORMAT
                                )
                            )
                            user_to_clean_message = clientsocket.recv(BUFFER).decode(
                                FORMAT
                            )
                            if clear_messages(user.username, user_to_clean_message):
                                clientsocket.send(
                                    "messages clear succefully".encode(FORMAT)
                                )
                            else:
                                clientsocket.send("wrong login".encode(FORMAT))
                        elif message_from_client == "STOP":
                            clientsocket.send(
                                options_dict[message_from_client].encode(FORMAT)
                            )
                            print("closed")
                            s.shutdown(socket.SHUT_RDWR)
                            s.close()
                        else:
                            clientsocket.send("command doesnt exist".encode(FORMAT))
                else:
                    clientsocket.send(
                        "Wrong login or password, choose again 1 or 2".encode(FORMAT)
                    )

            elif message_from_client == "1":
                clientsocket.send(
                    "Write new username and password for register".encode(FORMAT)
                )
                credentials = clientsocket.recv(BUFFER).decode(FORMAT).split(" ")
                while len(credentials) != 2:
                    clientsocket.send(
                        "Wrong format, write again username and password with space between for register".encode(
                            FORMAT
                        )
                    )
                    credentials = clientsocket.recv(BUFFER).decode(FORMAT).split(" ")
                user = User(credentials[0], credentials[1], 0)
                if db.add_new_user(user):
                    clientsocket.send("Now press 2 to log in".encode(FORMAT))
                else:
                    clientsocket.send(
                        "Login already exist choose again 1 or 2".encode(FORMAT)
                    )

