class User:
    def __init__(self, username, password, rights,):
        self.username = username
        self.password = password
        self.rights = rights

    def display(self):
        return f"name: {self.username}  password: {self.password}"

    def display_for_test(self):
        return {
            "username": self.username,
            "password": self.password,
            "rights": self.rights,
        }

    def __str__(self):
        return f"{self.username} {self.password} {self.rights}"

    def __eq__(self, other):
        return self.username == other.username and self.password == other.password
