class Server:
    server_commands_user = {
        "uptime": "return server life time",
        "info": "return information about server version",
        "help": "return commands list",
        "STOP": "connection closed (client and server)",
        "show users": "display all users",
        "display credential": "display credential",
        "change password": "change password",
        "send message": "sending message",
        "show messages": "showing messages",
    }

    server_commands_admin = {
        "remove": "deleting any account",
        "show users data": "display all users",
        "display credential": "display credential",
        "change password": "change password",
        "send message": "sending message",
        "show messages": "showing messages",
        "clear messages": "removes user messages",
    }

    server_commands_admin.update(server_commands_user)

    options_dict_user = {
        "help": server_commands_user,
        "info": "Server v.1.0.0",
        "uptime": 0,
        "STOP": "STOP",
        "users": "display all users",
    }

    options_dict_admin = {
        "help": server_commands_admin,
        "info": "Server v.1.0.0",
        "uptime": 0,
        "STOP": "STOP",
        "dupa": "dupaAdmin",
        "users": "display users",
    }
