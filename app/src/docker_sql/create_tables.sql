CREATE TABLE IF NOT EXISTS users (
                                id SERIAL PRIMARY KEY,
                                username text NOT NULL,
                                password text NOT NULL,
                                rights integer,
                                box text
                            );

CREATE TABLE IF NOT EXISTS messages (
                            id SERIAL PRIMARY KEY,
                            user_id integer NOT NULL,
                            message text NOT NULL,
                            sender text NOT NULL,
                            FOREIGN KEY (user_id) REFERENCES users (id)
                        );
