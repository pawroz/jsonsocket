from sql_database_tests import *
from string import *
import unittest
import names
import random


def random_char():
    letters = "".join(random.choice(ascii_letters) for _ in range(5))
    digits = ""
    for _ in range(10):
        value = random.randint(0, 10)
        digits += str(value)
    password = "".join(random.choice(digits + letters) for _ in range(7))
    return password


class Tests(unittest.TestCase):
    rand_name = names.get_first_name(gender="female")
    newest_message = "Some short sentence"

    def test_add_user(self):
        db.add_new_user(self.rand_name, random_char())
        self.assertEqual(db.get_users()[-1], db.get_specific_user(self.rand_name))

    def test_is_user_removed(self):
        user = db.get_specific_user(self.rand_name)
        db.delete_specific_user("Pablo", self.rand_name)
        db.get_specific_user(self.rand_name)
        self.assertNotIn(user, db.get_users())

    def test_if_message_receive(self):
        message = send_message("weronika", "Kuba", self.newest_message)
        self.assertEqual(message, True)

    def test_user_can_read_message(self):
        message = show_messages("Pablo")[-1]
        self.assertIn(self.newest_message, message)

    def test_is_box_cleaned(self):
        clear_messages("Pablo", "Kuba")
        self.assertEqual(db.get_count_of_messages("Kuba"), 0)


if __name__ == "__main__":
    unittest.main()

# ========================================================================
# def division(x):
#     return 10/x
# @pytest.mark.parametrize(
#     "value, expected",
#     [
#         (10, 1),
#         (5, 2),
#         (2, 5),
#         (0, ZeroDivisionError),
#         ("", TypeError),
#     ]
# )
# def test_division(value, expected):
#     if isinstance(expected, type):
#         with pytest.raises(expected):
#             division(value)
#     else:
#         assert division(value) == expected


# class TestClass:
#     def test_is_new_user_added(self):
#         user = User("test", "testhaslo", 0)
#         db.add_new_user(user)
#         print(user)
# existing_user = user.display_for_test()
# self.assertEqual(db.check_if_user_exist(user.username), existing_user)

# def test_is_user_removed(self):
#     user = User("test", "testhaslo", 0, {})
#     db.add_new_user(user)
#     db.upload_data()
#     db.remove_account(user.username)
#     self.assertNotIn(user.display_for_test(), db.data["USERS"])
#
# def test_user_can_read_message(self):
#     user = User("test2", "testhaslo", 0, {})
#     message = db.check_if_user_exist(user.username)
#     self.assertEqual(db.show_messages(user.username), message["box"])
#
# def test_user_message_arrived(self):
#     user = User("test2", "testhaslo", 0, {})
#     receiver = "Ania"
#     message = "jakas wiadomosc"
#     receivers_box = db.check_if_user_exist(receiver)["box"]
#     db.send_message(user.username, receiver, message)
#     self.assertEqual(db.show_messages(receiver), receivers_box)
#
# def test_is_box_cleaned(self):
#     user = User("test2", "testhaslo", 1, {})
#     receiver = "Ania"
#     message = "jakas wiadomoscw2"
#     db.send_message(user.username, receiver, message)
#     db.clear_messages(user.username, receiver)
#     self.assertEqual(db.show_messages(receiver), {})
#
# def test_check_credentials(self):
#     user = User("test_credential", "password", 0, {})
#     db.add_new_user(user)
#     db.upload_data()
#     existing_user = user.display_for_test()
#     self.assertEqual(db.check_if_user_exist(user.username), existing_user)
#     db.remove_account(user.username)
#
# def test_check_if_password_changed(self):
#     user = User("user_test_ password", "test_password", 0, {})
#     existing_user = user.display_for_test()
#     db.add_new_user(user)
#     db.upload_data()
#     db.change_password(user.username, user.password, "newpassword")
#     self.assertNotEqual(
#         db.check_if_user_exist(user.username)["password"], existing_user["password"]
#     )
#     db.remove_account(user.username)

#
# if __name__ == "__main__":
#     unittest.main()
